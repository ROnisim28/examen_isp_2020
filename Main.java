import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {

        Examen examen=new Examen();
    }
}


class Examen extends JFrame {

    JTextField textField;

    JButton saveButton;

    JTextArea textAreaData;

    BufferedReader bufferedReader;

    String filePath;



    public Examen() {

        setTitle("Examen");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        initialize();

        setSize(400, 400);
        setVisible(true);

    }


    public void initialize() {
        this.setLayout(null);


        textField = new JTextField();
        textField.setBounds(125, 15, 250, 25);



        saveButton = new JButton();
        saveButton.setText("Read");
        saveButton.setBounds(20, 15, 100, 25);
        saveButton.addActionListener(new Examen.saveButton_action());



        textAreaData=new JTextArea();
        textAreaData.setBounds(10,50,350,200);

        add(textField);
        add(saveButton);
        add(textAreaData);

    }


    public class saveButton_action implements ActionListener {

        String readData,read="";

        @Override
        public void actionPerformed(ActionEvent e) {

            filePath=textField.getText();


            try {
                bufferedReader = new BufferedReader(new FileReader(filePath));

                readData = bufferedReader.readLine();

                while (readData != null) {
                    read = read + readData + "\n";
                    readData = bufferedReader.readLine();
                }

                textAreaData.setText(read);

            } catch (FileNotFoundException ex) {
                textAreaData.setText("File does not exist");

            } catch (IOException ex) {

            }
        }



    }
}